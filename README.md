Gradle:
```bash
sudo mkdir /opt/gradle
curl -L https://downloads.gradle-dn.com/distributions/gradle-7.6-bin.zip -o /tmp/gradle-7.6-bin.zip
sudo unzip -d /opt/gradle /tmp/gradle-7.6-bin.zip
export PATH=$PATH:/opt/gradle/gradle-7.6/bin
gradle -v
gradle init (2,3->1,DSL:1,yes,4)
gradle tasks
tree -a .
./gradlew tasks --all
./gradlew build
./gradlew run
```

[Maven](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html):
```bash
# https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html
# https://tproger.ru/articles/maven-short-intro/
# http://spring-projects.ru/guides/maven/#initial
https://dlcdn.apache.org/maven/maven-3/3.8.7/binaries/apache-maven-3.8.7-bin.tar.gz
sudo mkdir /opt/maven
wget -O /tmp/apache-maven-3.8.7-bin.tar.gz https://dlcdn.apache.org/maven/maven-3/3.8.7/binaries/apache-maven-3.8.7-bin.tar.gz
sudo tar -zxf /tmp/apache-maven-3.8.7-bin.tar.gz -C /opt/maven
export PATH=$PATH:/opt/maven/apache-maven-3.8.7/bin
mvn -v
mvn archetype:generate \
	-DgroupId=javainsight \
	-DartifactId=app \
	-DarchetypeArtifactId=maven-archetype-quickstart \
	-DarchetypeVersion=1.4 \
	-DinteractiveMode=false
# Add to pom.xml: https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-api/5.9.1
cd app
mvn -N wrapper:wrapper
./mvnw validate
# mvn package
./mvnw package -Dmaven.test.skip
# mvn verify
java -classpath target/app-1.0-SNAPSHOT.jar javainsight.App
```

Simple program:
```bash
cat <<EOF > main.java
class HiProgram{
    public static void main(String[] args){
        System.out.println("Hey there!");}
}
EOF
javac main.java
java HiProgram
```

Links:
* https://ru.hexlet.io/courses/java-setup-environment/lessons/gradle-init/theory_unit
* http://spring-projects.ru/guides/gradle/#initial
* https://docs.gradle.org/current/userguide/war_plugin.html
* Основы JAVA: https://www.fandroid.info/tutorial-po-osnovam-yazyka-programmirovaniya-java-dlya-nachinayushhih/
* RESTful, gradle, maven, ldap ... http://spring-projects.ru/guides/
